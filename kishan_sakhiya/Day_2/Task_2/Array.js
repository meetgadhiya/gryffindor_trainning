'use strict'

function myArray1() {
    var a1;
    a1 = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k'];
    return a1;
}

function myArray2() {
    var a2;
    a2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
    return a2;
}

function displayArry(name) {
    document.write(name);
}
document.write("array 1 :");
var b = myArray1();
displayArry(b);
document.write("<br> array 2 :");
var c = myArray2();
displayArry(c);
document.write("<br> after concat : ");

var d = b.concat(c);
displayArry(d);
document.write("<br>");
document.write("<br>");
document.write("find 5 in array 2");
var e = c.indexOf(5, 0);
document.write("<br>");
displayArry(e);
document.write("<br>");

document.write("<br>");
document.write("push an element in array 1 ");
var f = b.push('l');
document.write("<br>");
displayArry(b);

document.write("<br>");
document.write("<br>");
document.write("pop an element in array 2 ");
document.write("<br>");
var g = c.pop();
document.write("<br>");
displayArry(c);