'use strict'

var a;
var b;
a = "Hello ";
b = "My name is Kishan";

document.write("String 1 : ");
document.write(a);
document.write("<br>");
document.write("<br>");

document.write("String 2 : ");
document.write(b);
document.write("<br>");
document.write("<br>");

var c = a.concat(b);
document.write("concat : " + c);
document.write("<br>");
document.write("<br>");

var d = b.charAt(6);
document.write("char at 6 in string 2 :" + d);
document.write("<br>");
document.write("<br>");

var e = b.includes('Kishan');
document.write("Is string 2 includes 'Kishan' ? ");
document.write("<br> ans : " + e);