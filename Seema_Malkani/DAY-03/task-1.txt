﻿// 13 feb 2018 

							                          TASK:1
													  
													  
Javascript :-
			JavaScript is a lightweight, interpreted programming language. 
			It is designed for creating network-centric applications. 
			It is complimentary to and integrated with Java. 
			JavaScript is very easy to implement because it is integrated with HTML. 
			It is open and cross-platform.

 1.1	Why to use js?
		- Easy to learn
		- No setup is required
		- JavaScript is used everywhere
		   ex :- web browser (Angular, React), server-side (Node),
				 mobile, desktop, games, Internet of Things, robotics, 
				 virtual reality, etc.
		 
 1.2 	Difference between java, js, Ecma script
		
		java & javascrpit
		Link:-
		http://freefeast.info/difference-between/difference-between-java-and-javascript-java-vs-javascript/
		
		EcmaScript ad javascrpit
		“ECMAScript is a specification.”
	    “JavaScript is an implementation of the ECMAScript standard.”
        “ECMAScript is standardized JavaScript.”
        “ECMAScript is a language.”
        “JavaScript is a dialect of ECMAScript.”
        “ECMAScript is JavaScript.”

		
1.3  Internal ad external scripts
	 
	 #Internal Script Example
	  
	  <html>
		<head>
		  <title>My First JavaScript code!!!</title>
		  <script type="text/javascript">
			// Create a Date Object
			var day = new Date();
			// Use getDay function to obtain todays Day.
			// getDay() method returns the day of the week as a number like 0 for Sunday, 1 for Monday,….., 5
			// This value is stored in today variable
			var today = day.getDay();
			// To get the name of the day as Sunday, Monday or Saturday, we have created an array named weekday and stored the values
			var weekday = new Array(7);
			weekday[0]="Sunday";
			weekday[1]="Monday";
			weekday[2]="Tuesday";
			weekday[3]="Wednesday";
			weekday[4]="Thursday";
			weekday[5]="Friday";
			weekday[6]="Saturday";
			// weekday[today] will return the day of the week as we want
			document.write("Today is " + weekday[today] + ".");
		  </script>
		</head>
		<body>
		</body>
		</html>
		
output: Today is Tuesday

		#External Script 
		
		Placing scripts in external files has some advantages:

		-It separates HTML and code
		-It makes HTML and JavaScript easier to read and maintain
		-Cached JavaScript files can speed up page loads
		
		Example:-
		1. External file: myscript.js
		
		function myFunction() {
		document.getElementById("demo").innerHTML = "Paragraph changed.";
		}
		
		-Eg:
		<script src="myScript.js"></script>
		