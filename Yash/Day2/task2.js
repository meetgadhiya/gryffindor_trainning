"use strict";
    function numberMethod(){
    //The JavaScript toString() Method
    var x = document.getElementById('1').value;
    document.getElementById("a").innerHTML = x.toString();
    //The toExponential() Method
    document.getElementById("b").innerHTML = x.toExponential()
    //The toFixed() Method
    document.getElementById("z").innerHTML = x.toFixed(0) + "<br>" +x.toFixed(2) + "<br>" +x.toFixed(4) + "<br>" +x.toFixed(6);
    }
    function stringMethod(){
    //String Length
    var txt = document.getElementById('2').value;
    document.getElementById("d").innerHTML = txt.length;
    //The substring() Method
    var res = txt.substring(7,13);
    document.getElementById("e").innerHTML = res;
    //To lowercase
    document.getElementById("f").innerHTML = txt.toLowerCase();
    //The charAt() Method
    document.getElementById("g").innerHTML = txt.charAt(3);
    }
    function dateMehod(){
    //The getTime() Method
    var d = new Date();
    document.getElementById("h").innerHTML = d.getTime();
    //Get full year
    document.getElementById("i").innerHTML = d.getFullYear();
    //The getDay() Method
    document.getElementById("j").innerHTML = d.getDay();
    }
    function arrayMethod(){
    //Converting Arrays to Strings
    var fruits = ["Banana", "Orange", "Apple", "Mango"];
    document.getElementById("k").innerHTML = fruits.toString();
    //Popping
    fruits.pop();
    document.getElementById("l").innerHTML = fruits;
    //Pushing
    fruits.push("Kiwi");
    document.getElementById("m").innerHTML = fruits;
    //Deleting Elements
    delete fruits[0];
    document.getElementById("n").innerHTML ="The first fruit is: " + fruits[0];
    }
